#' Create_Combines
#'
#' @description create a ComplexHeatmap
#' @param Compp Selected Comparision
#' @param grepp grep value
#' @param subset Gene of interest vector
#' @param TMM_all Table TMM
#' @param DE list of DE tables with comparision of interest
#' @param refmetabo metabolic vector
#' @param clusterized Boolean True or False
#' @param column_title title
#'
#' @return Combined Heatmap figure.
#'
#' @import ComplexHeatmap
#' @import stats
#' @importFrom dplyr arrange mutate select %>% filter
#'
#'
Create_Combines <- function(
    Compp="P_ECMvsPlastic_H",
    grepp="H_P[[:digit:]]ECM|H_P[[:digit:]]plastic",
    subset= c(),
    DE=DE,
    TMM_all = TMM_all,
    pvalue_=0.05,
    color_legend=list(),
    refmetabo=c()
    ,clusterized=T
    ,column_title="title"
    ,coloris=c("royalblue1", "white", "red2")
){

  reduce_subset<- subset[which(subset%in%rownames(DE[[paste0("S1_",Compp)]])&subset%in%rownames(DE[[paste0("S2_",Compp)]]))] #132
  matrix=TMM_all[grep(grepp,colnames(TMM_all),value=T)][reduce_subset,]
  matrix[which(rowSums(matrix)!=0),]->matrix

  without_0<-matrix%>%rownames()

  subDE_S1=DE[[paste0("S1_",Compp)]]%>%dplyr::filter(row.names(.)%in%without_0)#%>%arrange(padj) #132
  subDE_S2=DE[[paste0("S2_",Compp)]]%>%dplyr::filter(row.names(.)%in%without_0)#%>%arrange(padj) #132
  subDE_S1[without_0,]->subDE_S1
  subDE_S2[without_0,]->subDE_S2

  VAL_HEAT<-na.omit(as.matrix(cbind(
    scale_rows(matrix[grep("M1|P1",colnames(matrix))]),
    scale_rows(matrix[grep("M2|P2",colnames(matrix))]) )))
  foldchange_S1= subDE_S1%>%select(log2FoldChange)%>%dplyr::filter(row.names(.)%in%rownames(VAL_HEAT))#132
  foldchange_S2= subDE_S2%>%select(log2FoldChange)%>%dplyr::filter(row.names(.)%in%rownames(VAL_HEAT))#132

  pval_S1= subDE_S1%>% mutate(logpadj = -log(padj))%>%select(logpadj)#%>%arrange(padj)
  pval_S2= subDE_S2%>% mutate(logpadj = -log(padj))%>%select(logpadj) #%>%arrange(padj)


  NAMES<-stringr::str_extract(colnames(cbind(VAL_HEAT)),pattern='plastic|ECM')
  SOURIS<-stringr::str_extract(colnames(VAL_HEAT),pattern='P1|P2|M1|M2')
  CONDITION<- stringr::str_extract(colnames(VAL_HEAT),pattern='H|N')
  ha = HeatmapAnnotation(Type = NAMES,
                         Condition=CONDITION,
                         Mouse =SOURIS,
                         annotation_name_side = "right",
                         col=color_legend
                         )

  minVal=min(min(foldchange_S1),min(foldchange_S2) )
  maxVal=max(max(foldchange_S1),max(foldchange_S2) )

  col_fun=circlize::colorRamp2(
    c({if (minVal>0) -maxVal else minVal},
      0,
      {if (maxVal<0) -minVal else maxVal} ),
    coloris)


  pch_s1 = rep("*", nrow(foldchange_S1))
  pch_s1[!pval_S1>=-log(pvalue_)] = NA

  pch_s2 = rep("*", nrow(foldchange_S2))
  pch_s2[!pval_S2>=-log(pvalue_)] = NA


  row = HeatmapAnnotation(`metabolic ?`=rownames(VAL_HEAT)%in%refmetabo,
                          `log2FC S1`=anno_simple(foldchange_S1$log2FoldChange
                                                  ,pch ={if(!all(is.na(pch_s1)))  pch_s1 }
                                                  # ,pch = pch_s1,
                                                  ,col=col_fun,
                                                  pt_gp = grid::gpar(col = "black"), pt_size = unit(4, "mm")),
                          `log2FC S2`=anno_simple(foldchange_S2$log2FoldChange
                                                  ,pch ={if(!all(is.na(pch_s2)))  pch_s2 }
                                                  #,pch = pch_s2
                                                  , col=col_fun,
                                                  pt_gp = grid::gpar(col = "black"), pt_size = unit(4, "mm")),
                          col=color_legend#list(`metabolic ?`=c('TRUE'='seagreen3','FALSE'='navyblue'))
                          , which = "row"
  )


  sorted_ECMPLAST<-c(grep('ECM',colnames(VAL_HEAT)),grep('plastic',colnames(VAL_HEAT)))
  sorted_HN<-c(grep('H',colnames(VAL_HEAT)),grep('N',colnames(VAL_HEAT)))
  if(is.unsorted(sorted_ECMPLAST)){
    str=sorted_ECMPLAST
  }else {
    str=sorted_HN
  }

    ht_list=Heatmap(VAL_HEAT,
                    col=circlize::colorRamp2(c(min(VAL_HEAT),0,max(VAL_HEAT)), coloris),
                  name="Mouse Scaled", top_annotation = ha,
                  right_annotation = row , cluster_columns=clusterized, column_order=str
  )

  lgd_sig = Legend(title = "p-value",pch = "*", type = "points", labels = paste0("<=",pvalue_))
  lgd_pvalue = Legend(title = "log2FC", col_fun = col_fun)


  Figure<-draw(ht_list,row_title = "Genes Expressed scaled by mouse and clusterized",
       row_title_gp = grid::gpar(col = "black"),
       column_title = column_title,
       column_title_gp = grid::gpar(fontsize = 16), annotation_legend_list = list(lgd_pvalue,lgd_sig))

  return(Figure)

}
